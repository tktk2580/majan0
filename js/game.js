var tehai=[];
// 配列大小比較
function compareFunc(a, b) {
  return a - b;
}

// 萬子の追加button
for(let i=1; i<=9; i++){
  $('.button_ms' + i).click(function(){
    var img_count = $('.chose-img img').length;
    if(img_count < 14){
      var number=i;
      var imgs = $('.img_ms' + (number));
      var img_length = $('.img_ms' + number + ' img').length;
      if(img_length<4){
        var img_src='"img/manzu/p_ms' + number + '_1.gif"';
        imgs.append('<img src=' + img_src + '>');
        $(imgs).css('display', 'inline-block');
        var pai_number = i;
        tehai.push(pai_number);
        tehai.sort(compareFunc);
        console.log(tehai);
      }
    }
    if(img_count === 13){
      inputValue();
    }
  });
}

// ピンズの追加button
for(let i=1; i<=9; i++){
  $('.button_ps' + i).click(function(){
    var img_count = $('.chose-img img').length;
    if(img_count < 14){
      var number=i;
      var imgs = $('.img_ps' + (number));
      var img_length = $('.img_ps' + number + ' img').length;
      if(img_length<4){
        var img_src='"img/pinzu1/p_ps' + number + '_1.gif"';
        imgs.append('<img src=' + img_src + '>');
        $(imgs).css('display', 'inline-block');
        var pai_number = i+10;
        tehai.push(pai_number);
        tehai.sort(compareFunc);
        console.log(tehai);
      }
    }
    if(img_count === 13){
      inputValue();
    }
  });
}

// 索子の追加button
for(let i=1; i<=9; i++){
  $('.button_ss' + i).click(function(){
    var img_count = $('.chose-img img').length;
    if(img_count < 14){
      var number=i;
      var imgs = $('.img_ss' + (number));
      var img_length = $('.img_ss' + number + ' img').length;
      if(img_length<4){
        var img_src='"img/sozu1/p_ss' + number + '_1.gif"';
        imgs.append('<img src=' + img_src + '>');
        $(imgs).css('display', 'inline-block');
        var pai_number = i+20;
        tehai.push(pai_number);
        tehai.sort(compareFunc);
        console.log(tehai);
      }
    }
    if(img_count === 13){
      inputValue();
    }
  });
}

// 字牌の追加button
for(let i=1; i<=7; i++){
  $('.button_ji' + i).click(function(){
    var img_count = $('.chose-img img').length;
    if(img_count < 14){
      var number=i;
      var imgs = $('.img_ji' + (number));
      var img_length = $('.img_ji' + number + ' img').length;
      if(img_length<4){
        var img_src='"img/tupai_1/p_ji_' + number + '.gif"';
        imgs.append('<img src=' + img_src + '>');
        $(imgs).css('display', 'inline-block');
        var pai_number = i+90;
        tehai.push(pai_number);
        tehai.sort(compareFunc);
        console.log(tehai);
      }
    }
    if(img_count === 13){
      inputValue();
    }
  });
}

// 萬子削除
for(let i=1; i<=9; i++){
  $('.button_ms' + i + '-').click(function(){
    var number=i;
    var imgs = '.img_ms' + (number);
    var remove_element = (imgs) + ' > img:last';
    $(remove_element).remove();
    var pai_number = i;
    var remove_index = tehai.indexOf(pai_number);
    if (remove_index > -1) {
      tehai.splice(remove_index, 1);
    }
    console.log(tehai);
  });
}

// 筒子削除
for(let i=1; i<=9; i++){
  $('.button_ps' + i + '-').click(function(){
    var number=i;
    var imgs = '.img_ps' + (number);
    var remove_element = (imgs) + ' > img:last';
    $(remove_element).remove();
    var pai_number = i+10;
    var remove_index = tehai.indexOf(pai_number);
    if (remove_index > -1) {
      tehai.splice(remove_index, 1);
    }
    console.log(tehai);
  });
}

// 索子削除
for(let i=1; i<=9; i++){
  $('.button_ss' + i + '-').click(function(){
    var number=i;
    var imgs = '.img_ss' + (number);
    var remove_element = (imgs) + ' > img:last';
    $(remove_element).remove();
    var pai_number = i+20;
    var remove_index = tehai.indexOf(pai_number);
    if (remove_index > -1) {
      tehai.splice(remove_index, 1);
    }
    console.log(tehai);
  });
}

// 字牌削除
for(let i=1; i<=7; i++){
  $('.button_ji' + i + '-').click(function(){
    var number=i;
    var imgs = '.img_ji' + (number);
    var remove_element = (imgs) + ' > img:last';
    $(remove_element).remove();
    var pai_number = i+90;
    var remove_index = tehai.indexOf(pai_number);
    if (remove_index > -1) {
      tehai.splice(remove_index, 1);
    }
    console.log(tehai);
  });
}

function inputValue(){
  // 値を設定
  var send_tehai = tehai.join(',');
  console.log(send_tehai);
	document.tehai.tehai_value.value = send_tehai;
}
