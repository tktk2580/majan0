
<?php
  // 関数
  // 配列の値が連続かどうか
  function is_continuous_array(array $array){
    return range($array[0], $array[0] + count($array) - 1) === $array;
  }
  // 関数終わり
  // 頭もない
  // $tehai_array = array(1,2,3,11,12,13,21,22,23,81,82,83,93,92);
  // 国士無双
  // $tehai_array = array(1,9,11,19,21,29,81,82,83,91,92,93,94,94);
  // 4つ刻子
  // $tehai_array = array(1,1,1,11,11,11,22,22,22,83,83,83,94,94);
  // 3つ刻子(刻子上がり)(3)
  // $tehai_array = array(1,2,3,11,11,11,22,22,22,83,83,83,94,94);
  // 3つ刻子(3つ槓子)(順子でとってる二盃口でとる)(3)
  $tehai_array = array(1,1,1,1,2,2,2,2,3,3,3,3,94,94);
  // 3つ刻子(だが刻子上がりではない)(3)
  // $tehai_array = array(1,2,3,3,3,4,4,4,5,5,5,6,94,94);
  // 3つ刻子(上がっていない)(3)
  // $tehai_array = array(1,2,3,3,3,4,4,4,5,5,5,7,94,94);
  // 2つ刻子(2つ刻子として使用)(2)
  // $tehai_array = array(1,1,1,11,12,13,21,22,23,83,83,83,94,94);
  // 2つ刻子(2槓子)(2つ刻子として使用)(3)
  // $tehai_array = array(1,1,1,1,2,2,2,2,3,3,4,5,94,94);
  // 2つ刻子(1つ刻子として使用)(4)
  // $tehai_array = array(1,1,1,21,22,22,23,23,23,24,24,25,94,94);
  // $tehai_array = array(21,22,23,23,23,24,24,25,25,31,31,31,94,94);
  // 2つ刻子(1つ刻子として使用)あがれない(3)
  // $tehai_array = array(21,22,23,23,23,24,24,25,26,31,31,31,94,94);
  // 2つ刻子(全て順子で上がる)(3)
  // $tehai_array = array(21,22,23,23,23,24,24,25,25,25,26,27,94,94);
  // $tehai_array = array(21,21,22,22,22,23,23,23,24,31,32,33,94,94);
  // 2つ刻子(全て順子で上がる)(4)
  // $tehai_array = array(21,22,22,23,23,23,24,24,24,25,25,26,94,94);
  // 2つ刻子(全て順子で上がる)あがれない(4)
  // $tehai_array = array(21,22,22,23,23,23,24,24,24,25,25,27,94,94);
  // 1つ刻子(刻子として使用)(4)
  // $tehai_array = array(1,1,1,23,23,24,24,25,25,31,32,33,94,94);
  // 1つ刻子(槓子)(刻子として使用)(4)
  // $tehai_array = array(1,1,1,1,2,2,3,3,4,4,5,6,94,94);
  // 1つ刻子(刻子として使用)あがれない(4)
  // $tehai_array = array(1,1,1,23,23,24,24,25,25,31,32,34,94,94);
  // 1つ刻子(順子で上がる)(4)
  // $tehai_array = array(1,2,2,3,3,3,4,4,5,5,6,7,94,94);
  // 順子(5)
  // $tehai_array = array(1,1,11,11,12,12,13,13,14,15,15,16,16,17);
  
  $array_length = count($tehai_array);
  $tehai_limit = 14;
  $except_head_limit = $tehai_limit-2;

  // 国士無双チェック
  $kokushi_unique = array_unique($tehai_array);
  if(count($kokushi_unique) === 13){
    $kokushi_component = array(1,9,11,19,21,29,81,82,83,91,92,93,94);
    $check_kokushi = array_diff($tehai_array, $kokushi_component);
    if(count($check_kokushi)===0){
      var_dump("国士無双");
      return;
    }
  }
  // 頭候補のサーチ、カウント
  $head_count=0;
  $head_candidate=[];
  for($i=0; $i<$tehai_limit -1; $i++) {
    if($tehai_array[$i] === $tehai_array[$i+1]){
      // 計算量減らす余地あり、ifを使わずに$iを足してしまう。(コードがわかりにくくなるかもしれないが)
      if(!in_array($tehai_array[$i], $head_candidate)){
        $head_count+=1;
        $head_candidate[]=$tehai_array[$i];
      }
    }
  }

  // 頭候補がない場合return
  if($head_count === 0){
    var_dump("上がってません");
    return;    
  }

  // 頭を仮定して頭以外の牌の配列作成
  $head_except=[];
  for($i=0; $i<$head_count; $i++)  {
    $except=array_diff($tehai_array, array($head_candidate[$i]));
    $except_diff=$except_head_limit-count($except);
    for($e=0; $e<$except_diff; $e++){
      $except[]=$head_candidate[$i];
      sort($except);
    }
    $except=array_merge($except);
    $head_except[]=$except;
  }
  $tehai12_predict_array_count=count($head_except);

// 頭確認テスト
  var_dump("頭候補数");var_dump("<br>");
  var_dump($head_count);
  var_dump("<br>");
  var_dump($head_except[0]);
  var_dump("<br>");
  var_dump($head_except[1]);
  var_dump("<br>");
  var_dump($head_except[2]);
  var_dump("<br>");
// 頭確認テスト終わり


  // 手配の並び替え
  
  // $head_except配列にある中の一つの要素が上がれるか調査
  for($i=0; $i<$head_count; $i++){
    $tehai_sort=[];  
    // 頭を除いた12枚の手配の候補
    $tehai12_predict=$head_except[$i];
    // $tehai12_predict=$head_except[3];
    // とりあえず上でテスト
    var_dump($tehai12_predict);
    var_dump("<br>");
    var_dump($tehai12_predict[10]);
    var_dump("<br>");
    var_dump($tehai12_predict[11]);
    var_dump("<br>");
    $kotsu_count=0;
    $kotsu_candidate=[];
    // 刻子候補検索
    for($n=0; $n<$except_head_limit -2; $n++) {
      if($tehai12_predict[$n] === $tehai12_predict[$n+1] AND $tehai12_predict[$n+1] === $tehai12_predict[$n+2]) {
        // 計算量減らす余地あり、ifを使わずに$nを足してしまう。(コードがわかりにくくなるかもしれないが)
        if(!in_array($tehai12_predict[$n], $kotsu_candidate)){
          $kotsu_count+=1;
          $kotsu_candidate[]=$tehai12_predict[$n];
        }
      }
    }
    
    // 刻子確認テスト
    var_dump("刻子候補数");var_dump("<br>");
    var_dump($kotsu_count);
    var_dump("<br>");
    var_dump($kotsu_candidate);
    var_dump("<br>");
    // 刻子確認テスト

    if($kotsu_count===4){
      var_dump("4つ刻子");var_dump("<br>");
      var_dump("上がり");var_dump("<br>");
      return;
    }elseif($kotsu_count===3){
      var_dump("3つ刻子");var_dump("<br>");
      // 槓子(3つの刻子うちの一つが槓子)を持っている時に3つ刻子にしないで上がる方法はない前提
      $except_kotsu = array_diff($tehai12_predict, $kotsu_candidate);
      var_dump($except_kotsu);var_dump("<br>");
      var_dump(count($except_kotsu));var_dump("<br>");
      // 刻子として使わない場合、全て順子になる前提
      // 3つの刻子を刻子として上がるかどうか
      if(is_continuous_array($except_kotsu)){
        var_dump("三つ刻子として使って上がり");var_dump("<br>");
        $tehai_sort[]=$tehai12_predict;
        var_dump($tehai_sort);var_dump("<br>");
        return;
      }else{
        // 3つの刻子を刻子ではなく順子で上がる場合（並び替えも終えている（headは後で入れないとけない））4は4メンツのこと($how_many_mentsu)
        $how_many_mentsu = count($tehai12_predict)/3;
        for($n=0; $n<$how_many_mentsu; $n++){
          $lead_element=array_shift($tehai12_predict);
          $tehai_sort[]=$lead_element;
          // var_dump($tehai12_predict);var_dump("<br>");
          for($c=1; $c<3; $c++){
            $next_lead_element_search=array_search($lead_element+$c,$tehai12_predict);
            var_dump('aaaaaaaaaaaaa');var_dump("<br>");
            var_dump($next_lead_element_search);var_dump("<br>");
            // 次の奴がない場合はnullとなり以下でis_numericで弾かれる
            if(is_numeric($next_lead_element_search)){
              $next_lead_element=array_splice($tehai12_predict,$next_lead_element_search,1);
              $tehai_sort[]=$next_lead_element[0];
              var_dump("hhhhhhha");var_dump("<br>");
              var_dump($next_lead_element);var_dump("<br>");
              var_dump($tehai_sort);var_dump("<br>");
            }else{
              break 2;
            }
          }
        }
        if(count($tehai_sort)===12){
          var_dump("3刻子を刻子ではなく順子で使っている");var_dump("<br>");
          var_dump($tehai12_predict);var_dump("<br>");
          var_dump($tehai_sort);var_dump("<br>");
          return;
        }else{
          var_dump("このパターンは上がれません(3刻子)");var_dump("<br>");
        }
      }
    }elseif($kotsu_count===2){
      var_dump("2つ刻子");var_dump("<br>");
      $except_kotsu = $tehai12_predict;
      // 刻子を削除
      for($n=0; $n<$kotsu_count; $n++){
        for($c=0; $c<3; $c++){
          $kotsu_element = array_search($kotsu_candidate[$n], $except_kotsu);
          unset($except_kotsu[$kotsu_element]);
          $except_kotsu = array_values($except_kotsu);
        }
      }
      // $except_kotsu = array_diff($tehai12_predict, $kotsu_candidate);
      // var_dump($except_kotsu);var_dump("<br>");
      // var_dump(count($except_kotsu));var_dump("<br>");
      // 刻子候補を抜いた残りの2メンツ
      for($n=0; $n<2; $n++){
        $lead_element=array_shift($except_kotsu);
        $tehai_sort[]=$lead_element;
        for($c=1; $c<3; $c++){
          $next_lead_element_search=array_search($lead_element+$c,$except_kotsu);
          if(is_numeric($next_lead_element_search)){
            $next_lead_element=array_splice($except_kotsu,$next_lead_element_search,1);
            $tehai_sort[]=$next_lead_element[0];
          }else{
            var_dump("2刻子を刻子としては上がれない");var_dump("<br>");
            break 2;
          }
        }
      }
      // 2つの刻子以外の順子は全部で6だから
      if(count($tehai_sort)===6){
        for($n=0; $n<2; $n++){
          for($c=0; $c<3; $c++){
            $tehai_sort = array_merge($tehai_sort,array($kotsu_candidate[$n]));
          }
        }
        var_dump("2刻子を使って上がり");var_dump("<br>");
        var_dump($tehai_sort);var_dump("<br>");
        return;
      }else{
        var_dump("2刻子としては上がれない(2刻子)");var_dump("<br>");
        // 一つを刻子として使い、もう一つは順子として使う
        for($h=0; $h<$kotsu_count; $h++){
          $one_kotsu = array($kotsu_candidate[$h]);
          // 一つ刻子を抜いて9枚にしている
          $tehai_except_one_kotsu = array_diff($tehai12_predict, $one_kotsu);
          // 順子処理
          // 3メンツ($how_many_mentsu)
          $how_many_mentsu = count($tehai_except_one_kotsu)/3;
          $tehai_sort=[];
          for($n=0; $n<$how_many_mentsu; $n++){
            $lead_element=array_shift($tehai_except_one_kotsu);
            $tehai_sort[]=$lead_element;
            for($c=1; $c<3; $c++){
              $next_lead_element_search=array_search($lead_element+$c,$tehai_except_one_kotsu);
              // 次の奴がない場合はnullとなり以下でis_numericで弾かれる
              if(is_numeric($next_lead_element_search)){
                // var_dump("hhhhhhhh");var_dump("<br>");
                // var_dump($tehai_sort);var_dump("<br>");
                $next_lead_element=array_splice($tehai_except_one_kotsu,$next_lead_element_search,1);
                $tehai_sort[]=$next_lead_element[0];
              }else{
                var_dump("この刻子パターンは上がれません(2刻子1つを刻子)");var_dump("<br>");
                // for $cから抜ける
                break 2;
              }
            }
            // 計算減らせる。2刻子の一つ目で正しいメンツ完成したら二つ目調べない
            if(count($tehai_sort)===9){
              var_dump("aaaaaaaaaaaaaa");var_dump("<br>");
              var_dump($one_kotsu);var_dump("<br>");
              // 除いてた刻子を追加
              for($c=0; $c<3; $c++){
                $tehai_sort = array_merge($tehai_sort,$one_kotsu);
              }
              // 手配の並び替え(並び替えないでおく)
              // sort($tehai_sort);
              break 2;
            }
          }
        }
        // 2刻子を一つを刻子として使い、もう一つは順子として上がれるかどうか
        if(count($tehai_sort)===12){
          var_dump("2刻子を一つを刻子として使い、もう一つは順子として使っている");var_dump("<br>");
          var_dump($tehai_except_one_kotsu);var_dump("<br>");
          var_dump($tehai_sort);var_dump("<br>");
          return;
        }else{
          // 2刻子全て順子で上がれるかチェック
          var_dump("2刻子を一つを刻子、もう一つは順子の場合もあがれない");;var_dump("<br>");
          var_dump($tehai12_predict);var_dump("<br>");
          $how_many_mentsu = count($tehai12_predict)/3;
          $tehai_sort=[];
          for($n=0; $n<$how_many_mentsu; $n++){
            $lead_element=array_shift($tehai12_predict);
            $tehai_sort[]=$lead_element;
            for($c=1; $c<3; $c++){
              $next_lead_element_search=array_search($lead_element+$c,$tehai12_predict);
              // 次の奴がない場合はnullとなり以下でis_numericで弾かれる
              if(is_numeric($next_lead_element_search)){
                // var_dump("hhhhhhhh");var_dump("<br>");
                // var_dump($tehai_sort);var_dump("<br>");
                $next_lead_element=array_splice($tehai12_predict,$next_lead_element_search,1);
                $tehai_sort[]=$next_lead_element[0];
              }else{
                var_dump("この刻子パターンは上がれません(2刻子1つを刻子)");var_dump("<br>");
                // for $cから抜ける
                break 2;
              }
            }
          }
          if(count($tehai_sort)===12){
            var_dump("2刻子を全て順子として使う");var_dump("<br>");
            var_dump($tehai12_predict);var_dump("<br>");
            var_dump($tehai_sort);var_dump("<br>");
            return;
          }else{
            var_dump("この手配では上がれません(2刻子)");var_dump("<br>");
          }
        }
      }
    }elseif($kotsu_count===1){
      var_dump("1つ刻子");var_dump("<br>");
      var_dump($tehai12_predict);var_dump("<br>");
      $except_kotsu = $tehai12_predict;
      // 刻子を削除
      for($c=0; $c<3; $c++){
        $kotsu_element = array_search($kotsu_candidate[$n], $except_kotsu);
        unset($except_kotsu[$kotsu_element]);
        $except_kotsu = array_values($except_kotsu);
      }
      var_dump($except_kotsu);var_dump("<br>");
      $how_many_mentsu = count($except_kotsu)/3;
      $tehai_sort=[];
      for($n=0; $n<$how_many_mentsu; $n++){
        $lead_element=array_shift($except_kotsu);
        $tehai_sort[]=$lead_element;
        for($c=1; $c<3; $c++){
          $next_lead_element_search=array_search($lead_element+$c,$except_kotsu);
          // 次の奴がない場合はnullとなり以下でis_numericで弾かれる
          if(is_numeric($next_lead_element_search)){
            // var_dump("hhhhhhhh");var_dump("<br>");
            // var_dump($tehai_sort);var_dump("<br>");
            $next_lead_element=array_splice($except_kotsu,$next_lead_element_search,1);
            $tehai_sort[]=$next_lead_element[0];
          }else{
            var_dump("この刻子パターンは上がれません(1刻子,刻子として使う)");var_dump("<br>");
            // for $cから抜ける
            break 2;
          }
        }
      }
      for($c=0; $c<3; $c++){
        $tehai_sort = array_merge($tehai_sort,$kotsu_candidate);
      }
      if(count($tehai_sort)===12){
        var_dump("1刻子を刻子として使う");var_dump("<br>");
        var_dump($except_kotsu);var_dump("<br>");
        var_dump($tehai_sort);var_dump("<br>");
        return;
      }else{
        // 1刻子全て順子で上がれるかチェック
        var_dump("1刻子を刻子としてはあがれない");;var_dump("<br>");
        // var_dump($tehai12_predict);var_dump("<br>");
        $how_many_mentsu = count($tehai12_predict)/3;
        $tehai_sort=[];
        for($n=0; $n<$how_many_mentsu; $n++){
          $lead_element=array_shift($tehai12_predict);
          $tehai_sort[]=$lead_element;
          for($c=1; $c<3; $c++){
            $next_lead_element_search=array_search($lead_element+$c,$tehai12_predict);
            // 次の奴がない場合はnullとなり以下でis_numericで弾かれる
            if(is_numeric($next_lead_element_search)){
              // var_dump("hhhhhhhh");var_dump("<br>");
              // var_dump($tehai_sort);var_dump("<br>");
              $next_lead_element=array_splice($tehai12_predict,$next_lead_element_search,1);
              $tehai_sort[]=$next_lead_element[0];
            }else{
              var_dump("この順子パターンは上がれません(1刻子、全て順子)");var_dump("<br>");
              // for $cから抜ける
              break 2;
            }
          }
        }
        if(count($tehai_sort)===12){
          var_dump("1刻子を順子として使う");var_dump("<br>");
          var_dump($tehai12_predict);var_dump("<br>");
          var_dump($tehai_sort);var_dump("<br>");
          return;
        }else{
          var_dump("この手配では上がれません(1刻子)");var_dump("<br>");
        }
      }
    }else{
      $how_many_mentsu = count($tehai12_predict)/3;
      $tehai_sort=[];
      for($n=0; $n<$how_many_mentsu; $n++){
        $lead_element=array_shift($tehai12_predict);
        $tehai_sort[]=$lead_element;
        for($c=1; $c<3; $c++){
          $next_lead_element_search=array_search($lead_element+$c,$tehai12_predict);
          // 次の奴がない場合はnullとなり以下でis_numericで弾かれる
          if(is_numeric($next_lead_element_search)){
            // var_dump("hhhhhhhh");var_dump("<br>");
            // var_dump($tehai_sort);var_dump("<br>");
            $next_lead_element=array_splice($tehai12_predict,$next_lead_element_search,1);
            $tehai_sort[]=$next_lead_element[0];
          }else{
            var_dump("この順子パターンは上がれません(順子)");var_dump("<br>");
            // for $cから抜ける
            break 2;
          }
        }
      }
      if(count($tehai_sort)===12){
        var_dump("順子で上がり");var_dump("<br>");
        var_dump($tehai12_predict);var_dump("<br>");
        var_dump($tehai_sort);var_dump("<br>");
        return;
      }else{
        var_dump("この手配では上がれません");var_dump("<br>");
      }
    }
  }
  var_dump("fin");
?>