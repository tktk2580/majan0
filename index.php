<!DOCTYPE html>
<head>
  <meta lang="ja">
  <title>majan caluculate</title>
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
  <h1>上がり手役を選択してください。</h1>
  <!-- 萬子 -->
  <div class="img-box">
    <p class="chose_ms1"><img src="img/manzu/p_ms1_1.gif"></p>
    <button class=button_ms1>+</button>
    <button class=button_ms1->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms2"><img src="img/manzu/p_ms2_1.gif"></p>
    <button class=button_ms2>+</button>
    <button class=button_ms2->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms3"><img src="img/manzu/p_ms3_1.gif"></p>
    <button class=button_ms3>+</button>
    <button class=button_ms3->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms4"><img src="img/manzu/p_ms4_1.gif"></p>
    <button class=button_ms4>+</button>
    <button class=button_ms4->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms5"><img src="img/manzu/p_ms5_1.gif"></p>
    <button class=button_ms5>+</button>
    <button class=button_ms5->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms6"><img src="img/manzu/p_ms6_1.gif"></p>
    <button class=button_ms6>+</button>
    <button class=button_ms6->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms7"><img src="img/manzu/p_ms7_1.gif"></p>
    <button class=button_ms7>+</button>
    <button class=button_ms7->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms8"><img src="img/manzu/p_ms8_1.gif"></p>
    <button class=button_ms8>+</button>
    <button class=button_ms8->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ms9"><img src="img/manzu/p_ms9_1.gif"></p>
    <button class=button_ms9>+</button>
    <button class=button_ms9->-</button>
  </div>
  <br>
  <!-- 筒子 -->
  <div class="img-box">
    <p class="chose_ps1"><img src="img/pinzu1/p_ps1_1.gif"></p>
    <button class=button_ps1>+</button>
    <button class=button_ps1->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps2"><img src="img/pinzu1/p_ps2_1.gif"></p>
    <button class=button_ps2>+</button>
    <button class=button_ps2->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps3"><img src="img/pinzu1/p_ps3_1.gif"></p>
    <button class=button_ps3>+</button>
    <button class=button_ps3->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps4"><img src="img/pinzu1/p_ps4_1.gif"></p>
    <button class=button_ps4>+</button>
    <button class=button_ps4->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps5"><img src="img/pinzu1/p_ps5_1.gif"></p>
    <button class=button_ps5>+</button>
    <button class=button_ps5->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps6"><img src="img/pinzu1/p_ps6_1.gif"></p>
    <button class=button_ps6>+</button>
    <button class=button_ps6->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps7"><img src="img/pinzu1/p_ps7_1.gif"></p>
    <button class=button_ps7>+</button>
    <button class=button_ps7->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps8"><img src="img/pinzu1/p_ps8_1.gif"></p>
    <button class=button_ps8>+</button>
    <button class=button_ps8->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ps9"><img src="img/pinzu1/p_ps9_1.gif"></p>
    <button class=button_ps9>+</button>
    <button class=button_ps9->-</button>
  </div>
  <br>
  <!-- 索子 -->
  <div class="img-box">
    <p class="chose_ss1"><img src="img/sozu1/p_ss1_1.gif"></p>
    <button class=button_ss1>+</button>
    <button class=button_ss1->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss2"><img src="img/sozu1/p_ss2_1.gif"></p>
    <button class=button_ss2>+</button>
    <button class=button_ss2->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss3"><img src="img/sozu1/p_ss3_1.gif"></p>
    <button class=button_ss3>+</button>
    <button class=button_ss3->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss4"><img src="img/sozu1/p_ss4_1.gif"></p>
    <button class=button_ss4>+</button>
    <button class=button_ss4->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss5"><img src="img/sozu1/p_ss5_1.gif"></p>
    <button class=button_ss5>+</button>
    <button class=button_ss5->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss6"><img src="img/sozu1/p_ss6_1.gif"></p>
    <button class=button_ss6>+</button>
    <button class=button_ss6->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss7"><img src="img/sozu1/p_ss7_1.gif"></p>
    <button class=button_ss7>+</button>
    <button class=button_ss7->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss8"><img src="img/sozu1/p_ss8_1.gif"></p>
    <button class=button_ss8>+</button>
    <button class=button_ss8->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ss9"><img src="img/sozu1/p_ss9_1.gif"></p>
    <button class=button_ss9>+</button>
    <button class=button_ss9->-</button>
  </div>
  <br>
  <!-- 字牌 -->
  <div class="img-box">
    <p class="chose_ji_e"><img src="img/tupai_1/p_ji_1.gif"></p>
    <button class=button_ji1>+</button>
    <button class=button_ji1->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ji_s"><img src="img/tupai_1/p_ji_2.gif"></p>
    <button class=button_ji2>+</button>
    <button class=button_ji2->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ji_w"><img src="img/tupai_1/p_ji_3.gif"></p>
    <button class=button_ji3>+</button>
    <button class=button_ji3->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ji_n"><img src="img/tupai_1/p_ji_4.gif"></p>
    <button class=button_ji4>+</button>
    <button class=button_ji4->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ji_no"><img src="img/tupai_1/p_ji_5.gif"></p>
    <button class=button_ji5>+</button>
    <button class=button_ji5->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ji_h"><img src="img/tupai_1/p_ji_6.gif"></p>
    <button class=button_ji6>+</button>
    <button class=button_ji6->-</button>
  </div>
  <div class="img-box">
    <p class="chose_ji_c"><img src="img/tupai_1/p_ji_7.gif"></p>
    <button class=button_ji7>+</button>
    <button class=button_ji7->-</button>
  </div>
  <div class="chose-img">
    <p class="img_ms1"></p>
    <p class="img_ms2"></p>
    <p class="img_ms3"></p>
    <p class="img_ms4"></p>
    <p class="img_ms5"></p>
    <p class="img_ms6"></p>
    <p class="img_ms7"></p>
    <p class="img_ms8"></p>
    <p class="img_ms9"></p>
    <!-- <br> -->
    <p class="img_ps1"></p>
    <p class="img_ps2"></p>
    <p class="img_ps3"></p>
    <p class="img_ps4"></p>
    <p class="img_ps5"></p>
    <p class="img_ps6"></p>
    <p class="img_ps7"></p>
    <p class="img_ps8"></p>
    <p class="img_ps9"></p>
    <!-- <br> -->
    <p class="img_ss1"></p>
    <p class="img_ss2"></p>
    <p class="img_ss3"></p>
    <p class="img_ss4"></p>
    <p class="img_ss5"></p>
    <p class="img_ss6"></p>
    <p class="img_ss7"></p>
    <p class="img_ss8"></p>
    <p class="img_ss9"></p>
    <!-- <br> -->
    <p class="img_ji1"></p>
    <p class="img_ji2"></p>
    <p class="img_ji3"></p>
    <p class="img_ji4"></p>
    <p class="img_ji5"></p>
    <p class="img_ji6"></p>
    <p class="img_ji7"></p>
  </div>

  <form name="tehai" method="POST" action="<?php echo $_SERVER["PHP_SELF"]?>">
    <input type="hidden" name="tehai_value">
    <label>リーチ：</label>
    <select name="richi">
      <option value="">-</option>
      <option value="richi">リーチ</option>
      <option value="daburi">ダブリー</option>
    </select>
    <input type="checkbox" name="ippatsu" value="1">一発
    <input type="radio" name="agarikata" value="tsumo" checked="checked">ツモ
    <input type="radio" name="agarikata" value="ron">ロン
    <input type="checkbox" name="naki" value="1">鳴き
    <select name="machi">
      <option value="">待ち</option>
      <option value="ryanmen">リャンメン</option>
      <option value="kantyan">カンチャン</option>
      <option value="pentyan">ペンチャン</option>
      <option value="atama">アタマ</option>
    </select>
    <label>場風</label>
    <select name="bakaze">
      <option value=91>東</option>
      <option value=92>南</option>
    </select>
    <label>自風</label>
    <select name="zikaze">
      <option value=91>東</option>
      <option value=92>南</option>
      <option value=93>西</option>
      <option value=94>北</option>
    </select>
    <br>
    <label>役：</label>
    <input type="checkbox" name="sananko" value="1">三暗刻
    <input type="checkbox" name="rinsyan" value="1">嶺上開花
    <input type="checkbox" name="chankan" value="1">槍槓
    <input type="checkbox" name="sankantsu" value="1">三槓子
    <input type="checkbox" name="haitei" value="1">
    海底撈月(河底撈魚)
    <br>
    <label>特殊役：</label>
    <input type="checkbox" name="tenho" value="1">天和
    <input type="checkbox" name="chiho" value="1">地和
    <input type="checkbox" name="renho" value="1">人和
    <input type="checkbox" name="churen" value="1">九蓮宝燈
    <input type="checkbox" name="zyunseichuren" value="1">純正九蓮宝燈
    <input type="checkbox" name="sukantsu" value="1">四槓子
    <input type="checkbox" name="nagashimangan" value="1">流し満貫
    <input type="submit" value="チェック" name="btn">
  </form>

  <div>
    <p class="ihan">1飜</p>
    <p class="ryanhan">2飜</p>
    <p class="sanhan">3飜</p>
    <p class="mangan">満貫(4~5飜)</p>
    <p class="haneman">跳満(6~7飜)</p>
    <p class="baiman">倍満(8~10飜)</p>
    <p class="sanbaiman">三倍満(11~12飜)</p>
    <p class="yakuman">役満(13~飜)</p>
  </div>

  <div class="raisingsun">
    <h1>国士無双!!</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="tenho">
    <h1>天和</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="chiho">
    <h1>地和</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="renho">
    <h1>人和</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="churen">
    <h1>九蓮宝燈</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="zyunseichuren">
    <h1>純正九蓮宝燈</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="sukantsu">
    <h1>四槓子</h1>
    <img src="img/raisingsun.jpg">
  </div>
  <div class="nagashimangan">
    <h1>流し満貫</h1>
    <img src="img/tsumo.png">
  </div>
  <div class="tsumo">
    <h1>ツモ!!</h1>
    <img src="img/tsumo.png">
  </div>
  <div class="boo">
    <h1>上がってね〜よ、バーカ！！</h1>
    <img src="img/blood.jpg">
  </div>

  <?php
  //入力内容を表示する
  // var_dump($_POST["tehai_value"]);var_dump("<br>");
  $frend = explode(",",$_POST["tehai_value"]);
  // var_dump($frend);var_dump("<br>");
  foreach ($frend as $v) {
    $tehai_array[] = (int) $v;
  }

  require('judge.php');
  // 特殊役上がりの場合、飜の計算しない
  if(count($tehai_array)===14){
    require('yaku.php');
  }
  var_dump('最終何翻', $yaku_count);var_dump('<br>');

  require('calculate.php');
  ?>
<!-- 上がり結果画面 -->
  <?php
    var_dump('最終ステータス',$status);
    if($status==="raisingsun"){
      echo
        '<style>
          .raisingsun{
            display:block;
          }
        </style>';
    }elseif($status==="tenho"){
      echo
        '<style>
          .tenho{
            display:block;
          }
      </style>';
    }elseif($status==="chiho"){
      echo
        '<style>
          .chiho{
            display:block;
          }
      </style>';
    }elseif($status==="renho"){
      echo
        '<style>
          .renho{
            display:block;
          }
      </style>';
    }elseif($status==="churen"){
      echo
        '<style>
          .churen{
            display:block;
          }
      </style>';
    }elseif($status==="zyunseichuren"){
      echo
        '<style>
          .zyunseichuren{
            display:block;
          }
      </style>';
    }elseif($status==="sukantsu"){
      echo
        '<style>
          .sukantsu{
            display:block;
          }
      </style>';
    }elseif($status==="nagashimangan"){
      echo
        '<style>
          .nagashimangan{
            display:block;
          }
      </style>';
    }elseif($status==="ready" || $status==="chitoi"){
      echo
        '<style>
          .tsumo{
            display:block;
          }
      </style>';
    }elseif($status==="boo"){
      echo
        '<style>
          .boo{
            display:block;
          }
      </style>';
    }else{
      echo
        '<style>
        .raisingsun, .tsumo, .boo{
            display:none;
          }
      </style>';
    }
  ?>
  <script src = "js/game.js"></script>
</body>
